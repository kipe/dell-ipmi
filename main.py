import signal
import subprocess
from threading import Event

from decouple import config


def cast_sensor_config(sensor_config):
    return {
        sensor: list(
            sorted(
                [
                    {
                        "temperature": float(temperature),
                        "fan_speed": int(fan_speed),
                    }
                    for temperature, fan_speed in [point.split(":") for point in points]
                ],
                key=lambda x: x["temperature"],
            )
        )
        for sensor, *points in [
            conf.strip().split(",") for conf in sensor_config.split(";")
        ]
    }


STOP = Event()
IPMI_HOST = config("IPMI_HOST", cast=str)
IPMI_USER = config("IPMI_USER", cast=str)
IPMI_PASSWORD = config("IPMI_PASSWORD", cast=str)
SENSOR_CONFIG = config("SENSOR_CONFIG", cast=cast_sensor_config)


def parse_sensor(*sensor_info):
    match sensor_info:
        case (
            sensor,
            value,
            "rpm",
            state,
            _lo_norec,
            _lo_crit,
            _lo_nocrit,
            _up_nocrit,
            _up_crit,
            _up_norec,
        ):
            return {
                "type": "fan",
                "name": sensor,
                "value": float(value),
                "state": state,
            }

        case (
            sensor,
            value,
            "degrees c",
            state,
            _lo_norec,
            _lo_crit,
            _lo_nocrit,
            _up_nocrit,
            _up_crit,
            _up_norec,
        ):
            return {
                "type": "temperature",
                "name": sensor,
                "value": float(value),
                "state": state,
            }

        case (
            sensor,
            value,
            "watts",
            state,
            _lo_norec,
            _lo_crit,
            _lo_nocrit,
            _up_nocrit,
            _up_crit,
            _up_norec,
        ):
            return {
                "type": "power",
                "name": sensor,
                "value": float(value),
                "state": state,
            }

        case (
            sensor,
            value,
            "volts",
            state,
            _lo_norec,
            _lo_crit,
            _lo_nocrit,
            _up_nocrit,
            _up_crit,
            _up_norec,
        ):
            return {
                "type": "voltage",
                "name": sensor,
                "value": float(value),
                "state": state,
            }

        case (
            sensor,
            value,
            "amps",
            state,
            _lo_norec,
            _lo_crit,
            _lo_nocrit,
            _up_nocrit,
            _up_crit,
            _up_norec,
        ):
            return {
                "type": "current",
                "name": sensor,
                "value": float(value),
                "state": state,
            }


def read_sensors():
    sensors = [
        [x.lower().strip() for x in line.split("|")]
        for line in subprocess.check_output(
            [
                "ipmitool",
                "-I",
                "lanplus",
                "-H",
                f"{IPMI_HOST}",
                "-U",
                f"{IPMI_USER}",
                "-P",
                f"{IPMI_PASSWORD}",
                "sensor",
            ],
            encoding="utf-8",
        ).splitlines()
    ]
    for sensor in sensors:
        sensor_info = parse_sensor(*sensor)
        if sensor_info:
            yield sensor_info


def calculate_fan_speed(sensors):
    min_speed = -1
    sensors = list(sensors)

    for configured_sensor, configured_curve in SENSOR_CONFIG.items():
        try:
            maximum_value = max(
                [
                    sensor["value"]
                    for sensor in sensors
                    if sensor["name"] == configured_sensor
                ]
            )
        except ValueError:
            return 100

        upper_speed = next(
            (
                point["fan_speed"]
                for i, point in enumerate(configured_curve)
                if point["temperature"] >= maximum_value
            ),
            (len(configured_curve) - 1, 100),
        )

        min_speed = max(min_speed, upper_speed)

    if min_speed < 0:
        return 100
    return min_speed


def set_automatic_control(*_, **__):
    STOP.set()
    print("Resuming automatic control")
    subprocess.check_output(
        [
            "ipmitool",
            "-I",
            "lanplus",
            "-H",
            f"{IPMI_HOST}",
            "-U",
            f"{IPMI_USER}",
            "-P",
            f"{IPMI_PASSWORD}",
            "raw",
            "0x30",
            "0x30",
            "0x01",
            "0x01",
        ],
        encoding="utf-8",
    )


def set_manual_control():
    print("Starting manual control")
    subprocess.check_output(
        [
            "ipmitool",
            "-I",
            "lanplus",
            "-H",
            f"{IPMI_HOST}",
            "-U",
            f"{IPMI_USER}",
            "-P",
            f"{IPMI_PASSWORD}",
            "raw",
            "0x30",
            "0x30",
            "0x01",
            "0x00",
        ],
        encoding="utf-8",
    )


def set_fan_speed(fan_speed):
    print(f"Setting fan speed to {fan_speed}")
    subprocess.check_output(
        [
            "ipmitool",
            "-I",
            "lanplus",
            "-H",
            f"{IPMI_HOST}",
            "-U",
            f"{IPMI_USER}",
            "-P",
            f"{IPMI_PASSWORD}",
            "raw",
            "0x30",
            "0x30",
            "0x02",
            "0xff",
            f"{hex(fan_speed)}",
        ],
        encoding="utf-8",
    )


if __name__ == "__main__":
    import time

    for sig in [signal.SIGINT, signal.SIGTERM]:
        signal.signal(sig, set_automatic_control)

    set_manual_control()
    try:
        while not STOP.is_set():
            set_fan_speed(calculate_fan_speed(read_sensors()))
            time.sleep(2)
    finally:
        set_automatic_control()
