FROM python:3.10-alpine

ENV PYTHONUNBUFFERED=1

RUN apk add --no-cache ipmitool

RUN addgroup -S worker && adduser -S worker -G worker
USER worker
WORKDIR /home/worker

COPY --chown=worker:worker requirements.txt /tmp/requirements.txt

RUN pip3 install --user -r /tmp/requirements.txt && \
  rm -f /tmp/requirements.txt

COPY --chown=worker:worker main.py .

CMD [ "python3", "main.py" ]
